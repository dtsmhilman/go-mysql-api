package main

type Users struct {
	Id        string `form:"id" json:"1"`
	FirstName string `form:"firstname" json:"muhamad"`
	LastName  string `form:"lastname" json:"hilman"`
}

type Response struct {
	Status  int    `json:"1"`
	Message string `json:"success"`
	Data    []Users
}